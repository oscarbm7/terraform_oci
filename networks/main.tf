

resource "oci_core_subnet" "test_subnet" {
  #Required
  cidr_block = "${var.subnet_cidr_block}"
  compartment_id = "${var.compartment_ocid}"
  vcn_id = "${oci_core_vcn.test_vcn.id}"
  #Optional
  availability_domain = "${var.availability_domain}"
  //route_table_id      = "${oci_core_route_table.route_table_test.id}"
  security_list_ids   = ["${oci_core_vcn.test_vcn.default_security_list_id}"]
  dhcp_options_id = "${oci_core_vcn.test_vcn.default_dhcp_options_id}"

  display_name = "${var.SubNet_Name}"
  dns_label = "examplesubnet"
  depends_on = ["oci_core_vcn.test_vcn"]
}


resource "oci_core_vcn" "test_vcn" {
  #Required
  cidr_block = "${var.vcn_cidr_block}"
  compartment_id = "${var.compartment_ocid}"
  #Optional
  display_name = "${var.VCN_Name}"
  dns_label = "examplevcn"
}


resource "oci_core_internet_gateway" "internet_gateway_test" {
  compartment_id = "${var.compartment_ocid}"
  display_name   = "${var.Internet-Gateway-Test}"
  vcn_id = "${oci_core_vcn.test_vcn.id}"
  depends_on = ["oci_core_vcn.test_vcn"]
}

resource "oci_core_default_route_table" "route_table_test" {
  manage_default_resource_id = "${oci_core_vcn.test_vcn.default_route_table_id}"
  display_name   = "RouteTableTest"
  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = "${oci_core_internet_gateway.internet_gateway_test.id}"
  }
}

output "Mysubnet" {
  value = "${oci_core_subnet.test_subnet.id}"
}
