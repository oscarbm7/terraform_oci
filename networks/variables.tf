variable "compartment_ocid" {}
variable "availability_domain"{}

variable "vcn_cidr_block" {
  default = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  default = "10.0.0.0/24"
}

variable "VCN_Name" {
  default = "VCN-Test" // hostname
}

variable "SubNet_Name" {
  default = "SubNet-Test" // hostname
}

variable "Internet-Gateway-Test" {
  default = "Internet-Gateway-Test" // hostname
}