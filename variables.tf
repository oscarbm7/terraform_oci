### Geography ###
variable "tenancy_ocid" {}
variable "region" {}
variable "compartment_ocid" {}

### Credentials ###
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "ssh_public_key" {}

# Choose an Availability Domain
variable "AD" {
  default = "1"
}

