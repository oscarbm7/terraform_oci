# Configure the Oracle Cloud Infrastructure provider with an API Key
provider "oci" {
  tenancy_ocid     = "${var.tenancy_ocid}"
  user_ocid        = "${var.user_ocid}"
  fingerprint      = "${var.fingerprint}"
  private_key_path = "${var.private_key_path}"
  region           = "${var.region}"
  version          = ">= 3.43"
}

# Gets a list of Availability Domains
data "oci_identity_availability_domains" "ads" {
  compartment_id = "${var.tenancy_ocid}"
}
/*
module "oci_database_autonomous_database"{
  source = "./autonomous_database"
  compartment_ocid = "${var.compartment_ocid}"
}
*/

module "vcns"{
  source = "./networks"
  compartment_ocid = "${var.compartment_ocid}"
  availability_domain = "${lookup(data.oci_identity_availability_domains.ads.availability_domains[var.AD - 1],"name")}"
}


module "oci_instance"{
  source = "./instances"
  ssh_public_key = "${var.ssh_public_key}"
  subnet_ocid = "${module.vcns.Mysubnet}"
  tenancy_ocid = "${var.tenancy_ocid}"
  compartment_ocid = "${var.compartment_ocid}"
  availability_domain = "${lookup(data.oci_identity_availability_domains.ads.availability_domains[var.AD - 1],"name")}"
}





