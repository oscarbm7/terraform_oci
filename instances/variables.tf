variable "availability_domain"{}
variable "compartment_ocid" {}
variable "tenancy_ocid" {}
variable "ssh_public_key" {}
variable "subnet_ocid" {}


variable "InstanceName" {
  default = "Test-Instance" // hostname
}

variable "InstanceShape" {
  default = "VM.Standard2.1"
}

variable "InstanceOS" {
  default = "Oracle Linux"
}

variable "InstanceOSVersion" {
  default = "7.6"
}


