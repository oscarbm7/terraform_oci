

# Gets the OCID of the OS image to use
data "oci_core_images" "OLImageOCID" {
  compartment_id = "${var.compartment_ocid}"
  operating_system = "${var.InstanceOS}"
  operating_system_version = "${var.InstanceOSVersion}"

  filter {
    name   = "display_name"
    values = ["^.*${var.InstanceOSVersion}-[^G].*$"] # FIXME: ["^((?!GPU).)*$"]
    regex  = true
  }

}


resource "oci_core_instance" "Test-Instance" {
  availability_domain = "${var.availability_domain}"
  compartment_id      = "${var.compartment_ocid}"
  display_name        = "${var.InstanceName}"
  hostname_label      = "${var.InstanceName}"
  shape               = "${var.InstanceShape}"
  subnet_id           = "${var.subnet_ocid}"
  metadata = {
    ssh_authorized_keys = "${var.ssh_public_key}"
  }

  source_details {
    source_id   = "${lookup(data.oci_core_images.OLImageOCID.images[0], "id")}"
    source_type = "image"
  }

  create_vnic_details {
    subnet_id      = "${var.subnet_ocid}"
    hostname_label = "${var.InstanceName}"
    assign_public_ip = "true"
  }

  timeouts {
    create = "60m"
  }
}
